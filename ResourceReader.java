import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;

public final class ResourceReader implements Iterator<String> {
	private static final int RESFILE_MAGIC_INT = 0xee4d4910;

	private final int[] ids;
	private final String[] strings;
	private int currentPos;

	public ResourceReader(final String filename) throws IOException {
		try (
			final DataInputStream dis
				= new DataInputStream(new FileInputStream(filename))
		) {
			if (dis.readInt() != RESFILE_MAGIC_INT) {
				throw new IllegalArgumentException("Not a resource file.");
			}
			final int arrlen = dis.readInt() / 8;

			ids = new int[arrlen];
			final int[] offsets = new int[arrlen];
			strings = new String[arrlen - 1];

			for (int i = 0; i < arrlen; i++) {
				ids[i] = dis.readInt();
				offsets[i] = dis.readInt();
			}

			final byte[] buf = new byte[256];
			for (int i = 0; i < arrlen - 1; i++) {
				final int datalen
					= (offsets[i + 1] & 0xffffff) - (offsets[i] & 0xffffff);
				dis.readFully(buf, 0, datalen);
				strings[i] = new String(buf, 0, datalen, StandardCharsets.UTF_8);
			}
		}
	}

	public boolean hasNext() {
		return currentPos < strings.length;
	}

	public int nextIndex() {
		return ids[currentPos];
	}

	public String next() {
		return strings[currentPos++];
	}

	public static void main(final String[] args) {
		for (final String filename : args) {
			try {
				final ResourceReader r = new ResourceReader(filename);
				while (r.hasNext()) {
					final int id = r.nextIndex();
					System.out.println(id + "=\"" + r.next() + '"');
				}
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}
}
